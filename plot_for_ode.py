import numpy  as np
import matplotlib.pyplot as plt

data = np.loadtxt('data.txt')


x = data[:, 0]
y = data[:, 1]
plt.plot(x, y,'r--')
plt.title('Graph from data for Runge–Kutta’s Method')
plt.xlabel('X')
plt.ylabel('Y')
plt.grid()
plt.savefig("fig.png")
plt.show()