// y' = f(x,y)

package main 

import (
		"fmt"
		"math"
		"os"
		"os/exec" // for exec.Command : Python van Go
		)

func f(x float64,y float64) float64 { // y' = 2*(x^2 + y)
	return 2*(math.Pow(x,2) + y) 
}

func main() {
	x0 := 0.0
	y0 := 1.0 // y(0) = 1
	n := 10 // число шагов
	h := 0.1 // размер шага
	var k1, k2, k3, k4, x, y float64
	
	//data := make([][]float64, n)


	file, err := os.Create("data.txt")
     
    if err != nil{
        fmt.Println("Unable to create file:", err) 
        os.Exit(1) 
    }
    defer file.Close()


	for i := 1; i <= n; i++  {
		k1 = h*f(x0,y0)
		k2 = h*f(x0 + h/2, y0 + k1/2)
		k3 = h*f(x0 + h/2, y0 + k2/2)
		k4 = h*f(x0 + h, y0 + k3)
		y = y0 + (k1 + 2*k2 + 2*k3 + k4)/6
		x = x0 + h
		fmt.Printf("%.1f %.4f\n", x, y)
		//data[0] = append(data[0], x)
		//data[1] = append(data[1], y)

		_, err = file.WriteString(fmt.Sprintf("%.1f %.4f\n", x, y)) // writing...
        if err != nil {
            fmt.Printf("error writing string: %v", err)
        }

		x0 = x
		y0 = y
	} 

	//fmt.Printf("%.1f %.4f\n", data[0], data[1])

	
	script := "plot_for_ode.py"
	cmd := exec.Command("python", script)
	out, err := cmd.Output()
	fmt.Println(string(out))
	if err != nil {
	    fmt.Println("Error: ", err)
	} 
	
}