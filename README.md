![](https://gitlab.com/Antoniii/ode-van-go/-/raw/main/dy1.PNG)

![](https://gitlab.com/Antoniii/ode-van-go/-/raw/main/ideal-plot1.png)

![](https://gitlab.com/Antoniii/ode-van-go/-/raw/main/fig.png)

![](https://gitlab.com/Antoniii/ode-van-go/-/raw/main/img15.jpg)

## Sources

* [Метод Рунге — Кутты](https://ru.wikipedia.org/wiki/Метод_Рунге_—_Кутты)
* [Турчак Л.И. Основы численных методов. Под ред. В.В.Щенникова](https://gitlab.com/Antoniii/ode-van-go/-/blob/main/Turchak.pdf)
* [python-van-go](https://github.com/tonypithony/python-van-go)
* [Go: Run External Python script](https://stackoverflow.com/questions/27021517/go-run-external-python-script)
* [Запись в файл](https://metanit.com/go/tutorial/8.3.php)
* [Работа с массивами и срезами в Golang — append() и make()](https://golangify.com/array-append-make)
* [Initialize a 2d dynamic array in Go](https://stackoverflow.com/questions/23869717/initialize-a-2d-dynamic-array-in-go)
* [Динамические массивы](https://maxim218.gitbook.io/go/dinamicheskie-massivi)
* [Массивы из массивов в Golang](https://golangify.com/array)
* [Создание функции в Golang](https://golangify.com/func)
* [Основы Go: цикл for](https://golang-blog.blogspot.com/2019/01/go-for-loop.html)
